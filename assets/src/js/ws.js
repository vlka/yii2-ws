(function ($) {
    var methods = {
        init: function (obj) {
            return this.each(function () {
                var o = $.extend({
                    host: 'ws://' + window.location.host,
                    port: 2022,
                    eventNewPage: '_newPage_',
                    ioOptions: {
                        transports: [
                            'websocket'
                        ]
                    }
                }, obj);
                var t = $(this);
                var data = t.data('ws');
                if (!data) {
                    data = {};
                    data.socket = io(o.host + ':' + o.port, o.ioOptions);
                    data.socket.on('connect', function () {
                        data.socket.on('disconnect', function (reason) {
                            t.trigger('disconnect.ws', {socket: data.socket, reason: reason});
                        });
                        data.socket.on('error', function (error) {
                            t.trigger('error.ws', {socket: data.socket, error: error});
                        });
                        t.trigger('connect.ws', data.socket);
                    });
                    data.socket.on(o.eventNewPage, function () {
                        $.ajax({
                            url: o.getPageDataUrl,
                            method: 'post',
                            data: {
                                pageId: o.pageId
                            }
                        }).done(function(res){
                        }).fail(function(res){
                            console.log(res);
                        });
                    });

                    t.data('ws', data);
                }
            });
        },
        destroy: function () {
            return this.each(function () {
                var t = $(this);
                t.off('.ws');
                var data = t.data('ws');
                data.socket.close();
                delete(data);
                t.removeData('ws');
            });

        },
        on: function(event, func){
            return this.each(function () {
                var t = $(this);
                var data = t.data('ws');
                data.socket.on(event, func);
                t.data('ws', data);
            });
        }
    };

    $.fn.ws = function (method) {

        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Метод с именем ' + method + ' не существует для jQuery.tooltip');
        }

    };
})(jQuery);