<?php

namespace vlka\yii2ws\assets;


use yii\web\AssetBundle;

class WSAsset extends AssetBundle
{
    public $depends = [
        'yii\web\JqueryAsset',
        'vlka\yii2ws\assets\SocketIOAsset',
    ];

    public $sourcePath = '@vlka/yii2ws/assets/src';

    public $js = [
        'js/ws.js',
    ];
}