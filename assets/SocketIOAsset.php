<?php

namespace vlka\yii2ws\assets;


use yii\web\AssetBundle;

class SocketIOAsset extends AssetBundle
{
    public $sourcePath = '@bower/socket.io-client/dist';

    public $js = [
        'socket.io.js',
    ];
}