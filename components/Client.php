<?php
/**
 * Created by PhpStorm.
 * User: vl
 * Date: 21.07.18
 * Time: 8:17
 */

namespace vlka\yii2ws\components;


use vlka\yii2ws\WS;
use Yii;
use yii\base\Component;
use yii\helpers\Json;
use vlka\yii2ws\assets\WSAsset;
use yii\web\JsExpression;

class Client extends Component
{
    public $pluginOptions = [];
    public $selector = 'body';

    protected static $_pageId;
    protected static $_active = false;

    public function activate()
    {
        if(!static::$_active){
            static::$_pageId = Yii::$app->security->generateRandomString();
            $this->pluginOptions['ioOptions']['query'][WS::FIELD_PAGE_ID] = $this->pageId;
            $this->pluginOptions['pageId'] = $this->pageId;
            $pluginOptions = Json::encode($this->pluginOptions);
            $view = Yii::$app->view;
            WSAsset::register($view);
            $view->registerJs(<<<JS
                $('$this->selector').ws($pluginOptions);
JS
            );

            static::$_active = true;
        }
        return $this;
    }

    public function addListener($event, $func)
    {
        $this->activate();
        $view = Yii::$app->view;
        if(is_string($func)){
            $func = new JsExpression($func);
        }
        $view->registerJs(<<<JS
            $('$this->selector').on('connect.ws', function(e, socket){
                socket.on('$event', function(data){
                    $func
                }); 
            });
JS
);
        return $this;
    }

    public function getPageId()
    {
        return static::$_pageId;
    }
}