<?php

namespace vlka\yii2ws\components;


use yii\base\Component;

class Server extends Component
{
    protected $wsServer;

    public $port = 2022;
    public $options = [];

    public function init()
    {
        parent::init();
        $this->wsServer = new \vlka\ws\Server($this->port, $this->options);
        $this->wsServer->init();
    }

    public function addEventHandler($event, callable $func)
    {
        $this->wsServer->on($event, $func);
        return $this;
    }

    public function getWsServer()
    {
        return $this->wsServer;
    }
}