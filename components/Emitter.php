<?php

namespace vlka\yii2ws\components;


use yii\base\Component;

class Emitter extends Component
{
    public $port = 2022;
    public $salt = '';

    protected $emitter;

    public function init()
    {
        parent::init();
        $this->emitter = new \vlka\ws\Emitter($this->port, $this->salt);
    }

    public function emit($event, $data)
    {
        $this->emitter->emit($event, $data);
        return $this;
    }

    public function send($event, $data)
    {
        $this->emitter->send($event, $data);
    }

    public function to($room)
    {
        $this->emitter->to($room);
        return $this;
    }

    public function ask($event, $data)
    {
        return $this->emitter->ask($event, $data)[1];
    }
}