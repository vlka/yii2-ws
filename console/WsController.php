<?php

namespace vlka\yii2ws\console;


use vlka\yii2ws\components\Server;
use vlka\yii2ws\WS;
use Workerman\Worker;
use yii\console\Controller;

class WsController extends Controller
{
    public function actionIndex()
    {
        $server = (\Yii::$app->ws->className())::server();

        // For compatibility with workerman/worker
        global $argv;
        array_shift($argv);
        Worker::runAll();
    }

    /**
     * @return string
     */
    public function getHelpSummary()
    {
        return 'Start, stop and getting statuses and connections from WebSocket server';
    }

    /**
     * @param \yii\base\Action $action
     * @return string
     */
    public function getActionHelpSummary($action)
    {
        return <<<TEXT
yii ws start           Start WebSocket server
yii ws start -d        Start WebSocket server in DAEMON mode
yii ws status          Display WebSocket server status
yii ws connections     Display Connections
yii ws stop            Stop WebSocket server
yii ws restart         Restart WebSocket server
yii ws restart -d      Restart WebSocket server in DAEMON mode
yii ws reload          Reload WebSocket server
TEXT;
    }

    /**
     * @param \yii\base\Action $action
     * @return string
     */
    public function getActionHelp($action)
    {
        return $this->getHelpSummary();
    }

    /**
     * @param \yii\base\Action $action
     * @return array
     */
    public function getActionArgsHelp($action)
    {
        $str = <<<TEXT
            ----------------------------------------
            yii ws start           Start WebSocket server
            yii ws start -d        Start WebSocket server in DAEMON mode
            yii ws status          Display WebSocket server status
            yii ws connections     Display Connections
            yii ws stop            Stop WebSocket server
            yii ws restart         Restart WebSocket server
            yii ws restart -d      Restart WebSocket server in DAEMON mode
            yii ws reload          Reload WebSocket server
TEXT;

        return [
            'act' => [
                'comment' => "\r\n" . $str,
                'default' => null,
                'type' => null,
                'required' => true,
            ],
            '-d' => [
                'comment' => 'For commands "start" and "restart" run in DAEMON mode',
                'default' => null,
                'type' => null,
                'required' => false,
            ]
        ];
    }

}