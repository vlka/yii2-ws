Yii2 extansion, php websockets server, based on wokerman/woker
==============================================================
Yii2 extansion, php websockets server, based on wokerman/woker

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist vlka/yii2-ws "*"
```

or add

```
"vlka/yii2-ws": "dev-master"
```

to the require section of your `composer.json` file.


Usage
=

Console:
-
Start WebSocket server
```bash
./yii ws start
```
Start WebSocket server in DAEMON mode
```bash
./yii ws start -d
```
Display WebSocket status
```bash
./yii ws status
```
Display Connections
```bash
./yii ws connections
```
Stop WebSocket server
```bash
./yii ws stop
```
Restart WebSocket server
```bash
./yii ws restart
```
Restart WebSocket server in DAEMON mode
```bash
./yii ws restart -d
```
Reload WebSocket server
```bash
./yii ws reload
```
View. Add event listener:
-
```php
<?php
use vlka\yii2ws\components\WS;
/**
* @var $this yii\web\View
*/
$pageId = WS::client()->addListener('test', <<<JS
    console.log('test', data);
JS
)->pageId;
```

Controller. Emit event:
-
```php
<?php
namespace app\controllers;

use vlka\yii2ws\components\WS;
use yii\web\Controller;

class MyController extends Controller
{
    public function actionIndex()
    {
        WS::emitter()->emit('test', [
            'foo' => 'bar',
        ]);
    }
}