<?php

namespace vlka\yii2ws;

use yii\base\BootstrapInterface;
use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;
use yii\web\Application;

class Bootstrap implements BootstrapInterface
{

    /**
     * Bootstrap method to be called during application bootstrap stage.
     * @param Application $app the application currently running
     */
    public function bootstrap($app)
    {
        if(!isset($app->components['ws'])){
            $app->setComponents([
                'ws' => [
                    'class' => 'vlka\yii2ws\WS',
                ],
            ]);
        }
        if($app instanceof \yii\web\Application){

        } elseif ($app instanceof \yii\console\Application){
            $app->controllerMap['ws'] = 'vlka\yii2ws\console\WsController';
        }
    }
}