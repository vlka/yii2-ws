<?php

namespace vlka\yii2ws;


use Yii;
use yii\base\Component;
use yii\helpers\ArrayHelper;

class WS extends Component
{
    const FIELD_PAGE_ID = '_page_id_';
    protected $defaultClientOptions = [
        'class' => 'vlka\yii2ws\components\Client',
    ];
    protected $defaultServerOptions = [
        'class' => 'vlka\yii2ws\components\Server',
    ];
    protected $defaultEmitterOptions = [
        'class' => 'vlka\yii2ws\components\Emitter',
    ];

    public $clientOptions = [];
    public $serverOptions = [];
    public $emitterOptions = [];

    public function init()
    {
        parent::init();

        $this->clientOptions = ArrayHelper::merge($this->defaultClientOptions, $this->clientOptions);
        $this->serverOptions = ArrayHelper::merge($this->defaultServerOptions, $this->serverOptions);
        $this->emitterOptions = ArrayHelper::merge($this->defaultEmitterOptions, $this->emitterOptions);
    }

    public static function client()
    {
        return Yii::createObject(Yii::$app->ws->clientOptions);
    }

    public static function server()
    {
        return Yii::createObject(Yii::$app->ws->serverOptions);
    }

    public static function emitter()
    {
        return Yii::createObject(Yii::$app->ws->emitterOptions);
    }
}